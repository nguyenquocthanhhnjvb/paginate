<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\CustomerModel;
use Illuminate\Http\Response;
class CustomerController extends Controller
{
	
	public function index(Request $request)
	{
		$thisPage = $request->query('page_size',20);
		$search = $request->query('search');
		if ($search != null) {
			$customers = CustomerModel::where('phonenumber', 'like','%'.$search.'%')
			->orWhere('name', 'like','%'.$search.'%')
			->orWhere('email', 'like','%'.$search.'%')
			->paginate($thisPage)->withPath('?search='.$search.'&page_size='.$thisPage);
		}else{
			$customers = CustomerModel::paginate($thisPage)->withPath('?page_size='.$thisPage);
		}
		// dd($customers->count());
		return view('customer',compact('customers','thisPage','search'));

	}
}
