<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class CustomerModel extends Model
{
	public $timestamps = true;
	protected $table = 'customer';
}
