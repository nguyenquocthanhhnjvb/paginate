<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
	<script type="text/javascript">
		function getPageSize() {
			var select = document.getElementById("pageSize");
			var selectedValue = select.options[select.selectedIndex].dataset.href;
			window.location = selectedValue;	
		}
	</script>
</head>
<body>
	<div class="container mt-5">
		<div class="container-fluid">
			<form class="mb-0" method="GET" action="">
				<div class="md-form active-cyan-2 col-6 mb-3">
					<div class="input-group">
						<input id="search_input" name="search" value="{{$search}}" class="form-control" type="text" placeholder="Nhập từ khóa tìm kiếm" aria-label="Search" style="font-size: 15px">
						<div class="input-group-prepend">
							<button id="btn_search_submit" class="btn btn-primary waves-effect waves-light" style="color:#ffffff!important;background:!important;">Tìm kiếm</button>
						</div>
					</div>
					<input type="hidden" name="page_size" value="{{$thisPage}}">
				</div>
			</form>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th scope="col">STT</th>
						<th scope="col">Name</th>
						<th scope="col">PhoneNumber</th>
						<th scope="col">Email</th>
					</tr>
				</thead>
				<tbody>
					@if(isset($customers))
					@foreach($customers as $key => $customer)
					<tr>
						<th scope="row">{{$key+1}}</th>
						<td>{{$customer->name}}</td>
						<td>{{$customer->phonenumber}}</td>
						<td>{{$customer->email}}</td>
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>
			<br>
			<hr>
			@if($customers->total() > $thisPage)
			<div class="box-footer clearfix">
				<div class="row">
					<div class="col-md-5">
						<div class="row pagination-row">
							<div class="col-md-3">
								<select id="pageSize" name="page_size" class="pageSize form-control select-normal" onchange="getPageSize();"">
									<option {{$thisPage == 10 ? "selected" : ""}} data-href="{{url()->current()}}/?page_size=10">10</option>
									<option {{$thisPage == 20 ? "selected" : ""}} data-href="{{url()->current()}}/?page_size=20">20</option>
									<option {{$thisPage == 50 ? "selected" : ""}} data-href="{{url()->current()}}/?page_size=50">50</option>
									<option {{$thisPage == 100 ? "selected" : ""}} data-href="{{url()->current()}}/?page_size=100">100</option>
								</select>
							</div>
							<div class="col-md-9">
								<p class="label text-left normal-text">
									kết quả/trang
								</p>
							</div>
						</div>

					</div>
					<div class="col-md-7">
						<!-- Pagination -->
						<div class="float-right">
							<div class="no-margin text-center">
								{{$customers->links()}}
							</div>
						</div>
						<!-- / End Pagination -->
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>
</body>
</html>